import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as globals from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ServiceModule {

  constructor(private http:HttpClient) { }

  getAll(entity) {
    const url = globals.baseUrl + '/' + entity;
    return this.http.get(url, { headers: this.getHeaders() });
  }

  getById(entity, id) {
    const url = globals.baseUrl + '/' + entity + '/' + id;
    return this.http.get(url, { headers: this.getHeaders() });
  }

  saveOrUpdate(entity, data, save = true) {
    const url = globals.baseUrl + '/' + entity;
    if (save) {
      return this.http.post( url, JSON.stringify(data), { headers: this.getHeaders() }  )
    } else {
      return this.http.put( url, JSON.stringify(data), { headers: this.getHeaders() }  )
    }
  }

  postRequest(entity, data) {
    const url = globals.baseUrl + '/' + entity;
      return this.http.post( url, JSON.stringify(data), { headers: this.getHeaders() }  );
  }


  delete(entity, id) {
    const url = globals.baseUrl + '/' + entity;
    return this.http.delete(url + '/' + id, { headers: this.getHeaders() });
  }

  private getHeaders() {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }); 
    return headers;
  }
}
