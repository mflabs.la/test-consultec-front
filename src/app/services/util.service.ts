import { Injectable } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  @BlockUI() blockUI: NgBlockUI;

  constructor() { }

  
	blockUiStart(){
		this.blockUI.start();
	}
	
	blockUiStop(){
	    this.blockUI.stop();
	}

}
