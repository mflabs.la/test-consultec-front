import { Component, OnInit } from '@angular/core';
import { ServiceModule } from 'src/app/services/service.module';
import { UtilService } from 'src/app/services/util.service';
import { Customer } from 'src/app/models/customer';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 
  view:boolean = false;
  add:boolean = false;
  edit:boolean = false;
  submitted:boolean = false;

  customers:any = [];
  cities:any = [];
  states:any = [];

  customer: Customer;
  customerEdit:any;
  customerView : any;
  search = '*';

  constructor(private service: ServiceModule, private utilService: UtilService) {
    this.customer = new Customer();
   }

  ngOnInit() {
    this.getCustomer();
    this.getCities();
    this.getStatus();
  }

  editCustomer(c){
    this.customer = c;
    this.edit = true;
    this.add = !this.add;
  }

  viewCustomer(c){
    this.customerView = c;
    this.showViewCustomer();
  }

  show(){
    this.add = !this.add;
    this.edit = false;
    this.customer = new Customer();
  }

  showViewCustomer(){
    this.view = !this.view;
  }

  getCustomer(){
    this.utilService.blockUiStart();
    this.service.getAll('customer').subscribe(data => {
      this.utilService.blockUiStop();
      this.customers = data;
    });
  }

  getCities(){
    this.utilService.blockUiStart();
    this.service.getAll('city').subscribe(data =>{
      this.utilService.blockUiStop();
      this.cities = data;
    })
  }

  getStatus(){
    this.utilService.blockUiStart();
    this.service.getAll('get-status').subscribe(data =>{
      this.utilService.blockUiStop();
      this.states = data;
    });
  }

  addCustomer(form){
    this.submitted = true;
    if(form && !form.valid)
      return;
      if(!this.edit){
        this.utilService.blockUiStart();
        this.service.saveOrUpdate('customer', this.customer).subscribe(data =>{
          this.utilService.blockUiStop();
          Swal.fire(
            'Good job!',
            'the client has been saved successfully!',
            'success'
          );
          this.customer = new Customer();
          this.submitted = false;
          this.edit = true;
          this.getCustomer();
          this.show();
        }); 
      }
      if(this.edit){
        this.utilService.blockUiStart();
        this.service.saveOrUpdate('update-customer', this.customer, false).subscribe(data =>{
          this.utilService.blockUiStop();
          Swal.fire(
            'Good job!',
            'the client has been edited correctly!',
            'success'
          );
          this.customer = new Customer();
          this.submitted = false;
          this.getCustomer();
          this.show();
        });
      } 
  }

  inactiveCustomer(c){
    this.customer = c
    Swal.fire({
      title: 'Are you sure the delete customer?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {  
      if (result.value) {
        this.utilService.blockUiStart();
        this.service.postRequest('inactive', {status: 2, idcustomer: this.customer.id }).subscribe(data =>{
          this.utilService.blockUiStop();
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.getCustomer();
        });  
      }
    });
    
  }
}
