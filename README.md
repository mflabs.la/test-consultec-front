# TestConsultec

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.


# Pasos 

1. Clonar repositorio 
2. Instalar dependecias con `npm i`
3. Arrancar en local con `ng serve`
4. Abrir el navegador `http://localhost:4200/`

Nota: La conexión al api se encuentra `app/globals.ts`, si se desea se puede clonar el api en el siguiente repositorio `https://gitlab.com/mflabs.la/api-test-consultec`,
el api esta contruido en laravel y la base de datos MYSQL.



